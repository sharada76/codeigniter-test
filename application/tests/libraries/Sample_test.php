<?php

class Sample_test extends TestCase
{
    public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->library('sample');
        $this->obj = $this->CI->sample;
    }

    public function test_add()
    {
        $result = $this->obj->add(10, 2);
        $this->assertEquals(12, $result);
    }

    // テストケース multi(x, y)
    // public function test_multi()
    // {
    //     $result = $this->obj->multi(4, 6);
    //     $this->assertEquals(24, $result);
    // }

}